/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */

package de.triology.scm.plugins.bugzilla;

import java.io.Closeable;
import java.io.IOException;

import sonia.scm.repository.Repository;

/**
 * This object holds information about how to access the Bugzilla server.
 * @author Sebastian Sdorra
 * @author Jan Boerner, TRIOLOGY GmbH
 */
public class BugzillaBugRequest implements Closeable {

    //~--- fields ---------------------------------------------------------------

    /** Holds all the configuration parameters of a repository. */
    private final BugzillaConfiguration configuration;

    /** Handler for Bugzilla bugs. */
    private BugzillaHandler handler;

    /** Factory to create Bugzilla handlers. */
    private final BugzillaHandlerFactory handlerFactory;

    /** Password of to user who should access the server. */
    private final String password;

    /** Current repository. */
    private final Repository repository;

    /** E-Mail-Address of the user who should access the server. */
    private final String username;

    /**
     * Constructs the BugzillaBugRequest.
     *
     * @param pHandlerFactory The Bugzilla handler factory.
     * @param pUsername The user name of the currently logged in user.
     * @param pPassword The to the user belonging password.
     * @param pConfiguration The Bugzilla configuration.
     * @param pRepository The repository.
     */
    public BugzillaBugRequest(final BugzillaHandlerFactory pHandlerFactory, final String pUsername,
	    final String pPassword, final BugzillaConfiguration pConfiguration, final Repository pRepository)  {
	handlerFactory = pHandlerFactory;
	username = pUsername;
	password = pPassword;
	configuration = pConfiguration;
	repository = pRepository;
    }

    //~--- methods --------------------------------------------------------------

    /**
     * Logout from the handler.
     *
     * @throws IOException if there is an error while loggin out.
     */
    @Override
    public final void close() throws IOException {
	if (handler != null) {
	    try {
		handler.logout();
	    } catch (final BugzillaException ex) {
		throw new IOException("could not logout", ex);
	    }
	}
    }

    /**
     * Creates a new Instance of the BugzillaHandler.
     *
     * @return the new BugzillaHandler instance.
     *
     * @throws BugzillaConnectException if there are problems on creating process.
     */
    public final BugzillaHandler createBugzillaHandler() throws BugzillaConnectException {
	if (handler == null) {
	    handler = handlerFactory.createBugzillaHandler(configuration.getUrl(), username, password);
	}

	return handler;
    }

    //~--- get methods ----------------------------------------------------------

    /**
     * @return The configuration.
     */
    public final BugzillaConfiguration getConfiguration() {
	return configuration;
    }

    /**
     * @return The password.
     */
    public final String getPassword() {
	return password;
    }

    /**
     * @return The repository.
     */
    public final Repository getRepository() {
	return repository;
    }

    /**
     * @return The username.
     */
    public final String getUsername() {
	return username;
    }
}
