/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */

package de.triology.scm.plugins.bugzilla;

import java.text.MessageFormat;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.plugin.ext.Extension;
import sonia.scm.repository.ChangesetPreProcessorFactory;
import sonia.scm.repository.Repository;
import sonia.scm.util.HttpUtil;
import sonia.scm.util.Util;

import com.google.inject.Inject;

/**
 * Modify the changeset messages displayed by SCM-Manager by referencing
 * Bugzilla bug ids to the Bugzilla tickets.
 * 
 * @author Sebastian Sdorra
 * @author Jan Boerner, TRIOLOGY GmbH
 */
@Extension
public class BugzillaChangesetPreProcessorFactory implements ChangesetPreProcessorFactory {

    /** Make usage of the logging framework. */
    private static final Logger LOGGER = LoggerFactory.getLogger(BugzillaChangesetPreProcessorFactory.class);

    /** RegEx-Pattern of a bug id. */
    public static final String KEY_PATTERN = "([0-9]+)";

    /** Prefix to identify a bug number. */
    public static final String PROPERTY_BUGZILLA_PREFIX = "bugzilla.bug-prefix";

    /** Name of the Bugzilla-URL property. */
    public static final String PROPERTY_BUGZILLA_URL = "bugzilla.url";

    /** Replacement pattern for a Bugzilla bug id. */
    public static final String REPLACEMENT_LINK = "<a target=\"_blank\" href=\"{0}/show_bug.cgi?id=$1\">$0</a>";

    // ~--- fields ---------------------------------------------------------------

    /** The global context for this plugin. */
    private final BugzillaGlobalContext context;

    // ~--- constructors ---------------------------------------------------------

    /**
     * Constructs ...
     * 
     * 
     * @param pContext
     */
    @Inject
    public BugzillaChangesetPreProcessorFactory(final BugzillaGlobalContext pContext) {
	context = pContext;
    }

    // ~--- methods --------------------------------------------------------------

    /* (non-javadoc comment)
     * Creates a pre processor for changeset message modifications.
     * 
     * @param repository
     *            the current repository.
     * @return The preProcessor instance.
     */
    @Override
    public final BugzillaChangesetPreProcessor createPreProcessor(final Repository repository) {
	LOGGER.trace("Start creating Bugzilla changeset pre processor.");

	final BugzillaConfiguration configuration = BugzillaConfigurationResolver.resolve(context, repository);

	BugzillaChangesetPreProcessor cpp = null;
	String bugzillaUrl = configuration.getUrl();
	final String bugPrefix = configuration.getBugPrefix();

	if (Util.isNotEmpty(bugzillaUrl) && Util.isNotEmpty(bugPrefix)) {
	    bugzillaUrl = HttpUtil.getUriWithoutEndSeperator(bugzillaUrl);
	    final String replacementPattern = MessageFormat.format(REPLACEMENT_LINK, bugzillaUrl);
	    final String patternString = bugPrefix.concat(KEY_PATTERN);
	    final Pattern pattern;
	    if (configuration.isCaseSensitive()) {
		pattern = Pattern.compile(patternString);
	    } else {
		pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
	    }
	    cpp = new BugzillaChangesetPreProcessor(replacementPattern, pattern);
	} else {
	    LOGGER.debug("No changeset pre processer created, cause some of the needed parameters are empty.");
	}

	return cpp;
    }
}
