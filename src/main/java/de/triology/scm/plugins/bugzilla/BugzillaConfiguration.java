/**
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */



package de.triology.scm.plugins.bugzilla;

import java.util.Collections;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.Validateable;
import sonia.scm.repository.Repository;
import sonia.scm.util.Util;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;

/**
 * This is an Object that holds all configuration properties and values.
 * @author Sebastian Sdorra
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class BugzillaConfiguration implements Validateable {

  /** Auto close property name. */
  public static final String PROPERTY_AUTOCLOSE = "bugzilla.auto-close";

  /** Auto close keywords property name. */
  public static final String PROPERTY_AUTOCLOSEWORDS = "bugzilla.auto-close-words";

  /** Bug number prefix property name. */
  public static final String PROPERTY_BUGZILLA_BUG_PREFIX = "bugzilla.bug-prefix";

  /** If the bug number prefix is case sensitive. */
  public static final String PROPERTY_BUGZILLA_PREFIX_CASE_SENSITIVITY = "bugzilla.case-sensitive";

  /** Bugzilla URL property name. */
  public static final String PROPERTY_BUGZILLA_URL = "bugzilla.url";

  /** Update issues property name. */
  public static final String PROPERTY_UPDATEISSUES = "bugzilla.update-issues";

  /** Use SCM-Manager loginname to login to Bugzilla instead of eMail address. */
  public static final String PROPERTY_USE_LOGINNAME = "bugzilla.use-loginname";

  /** Passwort property name. */
  public static final String PROPERTY_PASSWORD = "bugzilla.password";

  /** Username property name */
  public static final String PROPERTY_USERNAME = "bugzilla.username";

  /** Value separator. */
  public static final String SEPARATOR = ",";

  /** the LOGGER for BugzillaConfiguration. */
  private static final Logger LOGGER = LoggerFactory.getLogger(BugzillaConfiguration.class);

  //~--- fields ---------------------------------------------------------------

  /** Auto close bugs? */
  @XmlElement(name = "auto-close")
  private boolean autoClose = false;

  /** Keywords for auto close. */
  @XmlElement(name = "auto-close-words")
  @XmlJavaTypeAdapter(XmlStringSetAdapter.class)
  private Set<String> autoCloseWords = null;

  /** Prefix to identify a number as Bugzilla bug number. */
  @XmlElement(name = "bug-prefix")
  private String bugPrefix;

  /** If the prefix is case sensitive. */
  @XmlElement(name = "case-sensitive")
  private boolean caseSensitive = false;

  /** Should Bugzilla bugs be updated on new push operations? */
  @XmlElement(name = "update-issues")
  private boolean updateIssues = false;

  /** URL to the Bugzilla server. */
  private String url = null;

  /**
   * Use loginname for login instead of eMail address.
   * @since 1.2
   */
  @XmlElement(name = "use-loginname")
  private boolean useLoginname = false;

  /** The username. */
  private String username = null;

  /** The password. */
  @XmlJavaTypeAdapter(XmlEncryptionAdapter.class)
  private String password = null;

  //~--- constructors ---------------------------------------------------------

  /**
   * Default contructor. Should only be used by jaxb.
   */
  public BugzillaConfiguration() {
  }

  /**
   * Constructs and initialize the configuration object.
   *
   * @param repository The current repository to get properties from.
   */
  public BugzillaConfiguration(final Repository repository) {
    LOGGER.trace("Construct configuration object from repository " + repository.getName());
    url = repository.getProperty(PROPERTY_BUGZILLA_URL);
    bugPrefix = repository.getProperty(PROPERTY_BUGZILLA_BUG_PREFIX);
    caseSensitive = getBooleanProperty(repository, PROPERTY_BUGZILLA_PREFIX_CASE_SENSITIVITY);
    updateIssues = getBooleanProperty(repository, PROPERTY_UPDATEISSUES);
    autoClose = getBooleanProperty(repository, PROPERTY_AUTOCLOSE);
    autoCloseWords = getSetProperty(repository, PROPERTY_AUTOCLOSEWORDS);
    useLoginname = getBooleanProperty(repository, PROPERTY_USE_LOGINNAME);
    username = repository.getProperty(PROPERTY_USERNAME);
    password = getEncryptedProperty(repository, PROPERTY_PASSWORD);
  }

  //~--- get methods ----------------------------------------------------------


  /**
   * @param repository
   * @param key
   *
   * @return The encrypted property.
   */
  private String getEncryptedProperty(final Repository repository, final String key) {
    String value = repository.getProperty(key);

    if (EncryptionUtil.isEncrypted(value)) {
      value = EncryptionUtil.decrypt(value);
    }

    return value;
  }

  /**
   * @return A list with keywords to identify an auto close operation.
   */
  public final Set<String> getAutoCloseWords() {
    return autoCloseWords;
  }

  /**
   * @return the bug prefix.
   */
  public final String getBugPrefix() {
    return bugPrefix;
  }

  /**
   * @return the Bugzilla URL.
   */
  public final String getUrl() {
    return url;
  }

  /**
   * @return The currently set password.
   */
  public String getPassword() {
    return password;
  }

  /**
   * @return The currently set username.
   */
  public String getUsername() {
    return username;
  }

  /**
   * @return true if auto close is enabled, false otherwise.
   */
  public final boolean isAutoCloseEnabled() {
    return isUpdateIssuesEnabled() && autoClose && Util.isNotEmpty(autoCloseWords);
  }

  /**
   * @return true, if issues should be updated.
   */
  public final boolean isUpdateIssuesEnabled() {
    return isValid() && updateIssues;
  }

  @Override
  public final boolean isValid() {
    return Util.isNotEmpty(url) && Util.isNotEmpty(bugPrefix);
  }

  /**
   * @param repository The repository to get property from.
   * @param key The property name to get.
   * @return a boolean value of the given property.
   */
  private boolean getBooleanProperty(final Repository repository, final String key) {
    boolean result = false;
    final String value = repository.getProperty(key);

    if (Util.isNotEmpty(value)) {
      result = Boolean.parseBoolean(value);
    }

    return result;
  }

  /**
   * @param repository
   * @param key
   *
   * @return The property as a Set object.
   */
  private Set<String> getSetProperty(final Repository repository, final String key) {
    Set<String> values;
    final String value = repository.getProperty(key);

    if (!Strings.isNullOrEmpty(value)) {
      //J-
      values = ImmutableSet.copyOf(
          Splitter.on(SEPARATOR).trimResults().omitEmptyStrings().split(value)
          );
      //J+
    }
    else {
      values = Collections.emptySet();
    }

    return values;
  }

  /**
   * @return the caseSensitive
   */
  public final boolean isCaseSensitive() {
    return caseSensitive;
  }

  /**
   * @return true if the loginname instead of the eMail address should be used for login.
   */
  public final boolean isUseLoginname() {
    return useLoginname;
  }
}
