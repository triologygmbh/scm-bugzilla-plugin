/**
 * (c) 2012 TRIOLOGY GmbH
 */
/**
 * This package contains classes for the SCM-Bugzilla-Plugin. Starting at BugzillaHandler.java for creating a new
 * plugin instance.
 * 
 * 
 * @author jboerner
 */
package de.triology.scm.plugins.bugzilla;