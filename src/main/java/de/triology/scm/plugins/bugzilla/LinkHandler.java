/**
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */


package de.triology.scm.plugins.bugzilla;


import sonia.scm.repository.Changeset;
import sonia.scm.repository.Repository;
import sonia.scm.url.UrlProvider;
import sonia.scm.url.UrlProviderFactory;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 *
 * @author Sebastian Sdorra
 */
public class LinkHandler {

    //~--- fields ---------------------------------------------------------------

    /** Field description */
    private final UrlProvider restUrlProvider;

    /** Field description */
    private final UrlProvider wuiUrlProvider;

    /**
     * Constructs ...
     *
     *
     * @param wuiUrlProvider
     * @param restUrlProvider
     */
    @Inject
    public LinkHandler(
	    @Named(UrlProviderFactory.TYPE_WUI) final UrlProvider wuiUrlProvider,
	    @Named(UrlProviderFactory.TYPE_RESTAPI_XML) final UrlProvider restUrlProvider) {
	this.wuiUrlProvider = wuiUrlProvider;
	this.restUrlProvider = restUrlProvider;
    }

    //~--- get methods ----------------------------------------------------------

    /**
     * Method description
     *
     *
     * @param repository
     * @param changeset
     *
     * @return
     */
    public String getDiffRestUrl(final Repository repository, final Changeset changeset) {
	return restUrlProvider.getRepositoryUrlProvider().getDiffUrl(
		repository.getId(), changeset.getId());
    }

    /**
     * Method description
     *
     *
     * @param repository
     * @param changeset
     *
     * @return
     */
    public String getDiffUrl(final Repository repository, final Changeset changeset) {
	return wuiUrlProvider.getRepositoryUrlProvider().getDiffUrl(
		repository.getId(), changeset.getId());
    }

    /**
     * Method description
     *
     *
     * @param repository
     *
     * @return
     */
    public String getRepositoryUrl(final Repository repository) {
	return wuiUrlProvider.getRepositoryUrlProvider().getDetailUrl(
		repository.getId());
    }
}
