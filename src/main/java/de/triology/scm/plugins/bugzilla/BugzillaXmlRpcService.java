/**
 * 
 */
package de.triology.scm.plugins.bugzilla;

import java.net.URL;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;

import sonia.scm.util.HttpUtil;

/**
 * Provides methods to talk to the Bugzilla API.
 * @author jboerner
 *
 */
public class BugzillaXmlRpcService extends BugzillaAbstractRpcCall {

    /**
     * Connects to the Bugzilla server using default URL.
     */
    public BugzillaXmlRpcService() {
	super("Bugzilla.version", null);
    }

    /**
     * Creates a new BugzillaXmlRpcService connection to given interface.
     * @param url The URL to the Bugzilla interface.
     */
    public BugzillaXmlRpcService(final URL url) {
	super("Bugzilla.version", url);
    }

    /**
     * Creates a new BugzillaXmlRpcService.
     * @param pCommand The command to execute.
     */
    public BugzillaXmlRpcService(final String pCommand) {
	super(pCommand, null);
    }

    /**
     * 
     * @param login The login name.
     * @param password The password.
     * @return The user ID.
     * @throws XmlRpcException if there was an error executing the request.
     */
    public final String login(final String login, final String password) throws XmlRpcException {
	clearParameters();
	setCommand("User.login");
	setParameter("login", login);
	setParameter("password", password);
	setParameter("remember", true);
	final Map<?, ?> result = execute();
    Object token = result.get("token");
    if (token != null) {
        setLoginToken(String.valueOf(token));
    }
	return String.valueOf(result.get("id"));
    }

    /**
     * Log out currently logged in user from the bugzilla system.
     * @throws XmlRpcException If there was an error executing the logout command.
     */
    public final void logout() throws XmlRpcException {
	clearParameters();
	setCommand("User.logout");
	execute();
	((XmlRpcBugzillaHttpTransport) getClient().getTransportFactory().getTransport()).clearCookieCache();
    setLoginToken(null);
    }

    /**
     * Add a comment to a bug.
     * @param bugId The Bug ID.
     * @param comment The comment.
     * @throws XmlRpcException Exception, when there was an error on executing the command.
     */
    public final void addComment(final int bugId, final String comment) throws XmlRpcException {
	clearParameters();
	setCommand("Bug.add_comment");
	setParameter("id", bugId);
	setParameter("comment", HttpUtil.decode(comment));
	execute();
    }

    /**
     * Mark the given bug as closed with given resolution.
     * @param bugId The bug ID.
     * @param resolution The resolution why the bug is closed.
     * @throws XmlRpcException Exception, when there was an error on executing the command.
     */
    public final void closeBug(final int bugId, final String resolution) throws XmlRpcException {
	clearParameters();
	setCommand("Bug.update");
	setParameter("ids", bugId);
	setParameter("status", "RESOLVED");
	setParameter("resolution", resolution);
	execute();
    }

}
