/**
 * Copyright (c) 2012, TRIOLOGY GmbH
 * 
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */

package de.triology.scm.plugins.bugzilla;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.repository.Repository;
import sonia.scm.security.CipherUtil;
import sonia.scm.user.User;
import sonia.scm.util.AssertUtil;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.Singleton;

//~--- JDK imports -----------------------------------------------------------

/**
 * Manages requests to the Bugzilla server.
 * 
 * @author Sebastian Sdorra
 * @author Jan Boerner, TRIOLOGY GmbH
 */
@Singleton
public class BugzillaBugRequestFactory {

  /** SCM Credentials attribute name. */
  public static final String SCM_CREDENTIALS = "SCM_CREDENTIALS";

  /** The logger for BugzillaBugRequestFactory. */
  private static final Logger LOGGER = LoggerFactory.getLogger(BugzillaBugRequestFactory.class);

  // ~--- fields ---------------------------------------------------------------

  /** Field description. */
  private final BugzillaHandlerFactory handlerFactory;

  // ~--- constructors ---------------------------------------------------------

  /**
   * Constructs the BugzillaBugRequestFactory.
   * 
   * @param pHandlerFactory The Bugzilla handler factory.
   * @param pSecurityContextProvider the WebSecurityContext provider.
   */
  @Inject
  public BugzillaBugRequestFactory(final BugzillaHandlerFactory pHandlerFactory) {
    handlerFactory = pHandlerFactory;
  }

  // ~--- methods --------------------------------------------------------------

  /**
   * Creates a Bugzilla bug request to the bugzilla server.
   * 
   * @param configuration The Bugzilla configuration.
   * @param repository The repository.
   * 
   * @return the new BugzillaRequest.
   */
  public final BugzillaBugRequest createRequest(final BugzillaConfiguration configuration,
      final Repository repository) {
    String loginname = configuration.getUsername();
    String password = configuration.getPassword();

    if (Strings.isNullOrEmpty(loginname)) {
      if (LOGGER.isTraceEnabled()) {
        LOGGER.trace("no username configured, use current credentials");
      }

      String[] credentials;
      try {
        final int nbrOfItems = configuration.isUseLoginname() ? 2 : 3;
        credentials = getUserCredentials(nbrOfItems);
        if (configuration.isUseLoginname()) {
          loginname = credentials[0];
        } else {
        // the email address is stored at the last position
          loginname = credentials[2];

        }
        password = credentials[1];
      } catch (final BugzillaException e) {
        // TODO Auto-generated catch block
        LOGGER.error("Can't create Request to Bugzilla.", e);
      }

    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("create bugzilla bug request for user {}", loginname);
    }

    return new BugzillaBugRequest(handlerFactory, loginname, password, configuration, repository);
  }

  // ~--- get methods ----------------------------------------------------------
  /**
   * Read in an returns the credentials String of the currently logged in
   * user. The format is username:password:email
   * 
   * @return the users credentials.
   */
  private String getCredentials() {
    String credentials = null;
    final Subject subject = SecurityUtils.getSubject();
    final Session session = subject.getSession();
    if (session != null) {
      // load user and password
      credentials = (String) session.getAttribute(SCM_CREDENTIALS);
      AssertUtil.assertIsNotEmpty(credentials);
      credentials = CipherUtil.getInstance().decode(credentials);
      // get email address
      final PrincipalCollection principals = subject.getPrincipals();
      final User user = principals.oneByType(User.class);

      final String mail = user.getMail();
      AssertUtil.assertIsNotEmpty(mail);
      credentials = credentials.concat(":").concat(mail);

    }
    return credentials;
  }

  /**
   * Extracts username and password from a credentials string.
   * 
   * @param nbrOfItems Minimum number of items expected.
   * @return An array containing username, password and eMail of the currently logged in user.
   * @throws BugzillaException if the credentials array is not complete.
   */
  private String[] getUserCredentials(final int nbrOfItems) throws BugzillaException {
    final String credentialsString = getCredentials();
    final String[] credentialsArray = credentialsString.split(":");

    if (credentialsArray.length < nbrOfItems) {
      throw new BugzillaException("non valid credentials found");
    }

    return credentialsArray;
  }
}
