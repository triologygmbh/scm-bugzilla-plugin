/**
 * Copyright (c) 2012, TRIOLOGY GmbH
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */



package de.triology.scm.plugins.bugzilla;

//~--- non-JDK imports --------------------------------------------------------

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.util.HttpUtil;
//~--- JDK imports ------------------------------------------------------------

/**
 *
 * @author Sebastian Sdorra
 */
public class XmlRpcBugzillaHandlerFactory implements BugzillaHandlerFactory {

    /** Path to the XML-RPC interface. */
    public static final String PATH_XML_RPC_SERVICE = "/xmlrpc.cgi";

    /** the logger for XmlRpcBugzillaHandlerFactory. */
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlRpcBugzillaHandlerFactory.class);

    //~--- methods --------------------------------------------------------------

    @Override
    public final BugzillaHandler createBugzillaHandler(final String urlString, final String username,
	    final String password) throws BugzillaConnectException {
	BugzillaHandler handler = null;

	try {
	    final URL url = createXmlRpcUrl(urlString);

	    if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("connect to bugzilla {} as user {}", url, username);
	    }

	    final BugzillaXmlRpcService service = new BugzillaXmlRpcService(url);
	    handler = new XmlRpcBugzillaHandler(service, urlString);
	    handler.login(username, password);
	} catch (final Exception ex) {
	    throw new BugzillaConnectException("could not connect to bugzilla instance at ".concat(urlString), ex);
	}

	return handler;
    }

    /**
     * Creates an XML-RPC Bugzilla-URL.
     *
     *
     * @param url The URL to the Bugzilla-Server.
     * @return An URL object to the Bugzilla XML-RPC interface.
     * @throws MalformedURLException if not a valid URL.
     */
    private URL createXmlRpcUrl(final String url) throws MalformedURLException {
	final String bugzillaUrl = HttpUtil.getUriWithoutEndSeperator(url).concat(PATH_XML_RPC_SERVICE);
	return new URL(bugzillaUrl);
    }
}
