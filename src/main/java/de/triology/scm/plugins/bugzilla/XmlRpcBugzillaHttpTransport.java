/**
 * 
 */
package de.triology.scm.plugins.bugzilla;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcRequest;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientException;
import org.apache.xmlrpc.client.XmlRpcSunHttpTransport;

/**
 * Extends the XmlRpcSunHttpTransport implementation with cookie support.
 * @author jboerner
 *
 */
public class XmlRpcBugzillaHttpTransport extends XmlRpcSunHttpTransport {
    /** Very simple cookie storage. */
    private static final Map<String, String> COOKIES = new LinkedHashMap<String, String>();
    /** The connection. */
    private URLConnection conn;

    /**
     * Creates a new transport instance.
     * @param client The client controlling this instance.
     */
    public XmlRpcBugzillaHttpTransport(final XmlRpcClient client) {
	super(client);
    }

    @Override
    protected final URLConnection newURLConnection(final URL pURL) throws IOException {
	conn = super.newURLConnection(pURL);
	return conn;
    }

    @Override
    protected final void initHttpHeaders(final XmlRpcRequest pRequest) throws XmlRpcClientException {
	super.initHttpHeaders(pRequest);
	setCookies();
    }

    @Override
    protected final void close() throws XmlRpcClientException {
	super.close();
	getCookies(conn);
    }

    /**
     * Set cookies to the request headers.
     */
    private void setCookies() {
	String cookieString = "";
	for (final String cookieName : COOKIES.keySet()) {
	    cookieString += "; " + cookieName + "=" + COOKIES.get(cookieName);
	}
	if (cookieString.length() > 2) {
	    setRequestHeader("Cookie", cookieString.substring(2));
	}
    }

    /**
     * Get cookies from the connection.
     * @param pConn the actual connection.
     */
    private void getCookies(final URLConnection pConn) {
	int i = 1;
	String headerName;
	while ((headerName = pConn.getHeaderFieldKey(i)) != null) {
	    if (headerName.equals("Set-Cookie")) {
		String cookie = pConn.getHeaderField(i);
		cookie = cookie.substring(0, cookie.indexOf(';'));
		final String cookieName = cookie.substring(0, cookie.indexOf('='));
		final String cookieValue = cookie.substring(cookie.indexOf('=') + 1, cookie.length());
		COOKIES.put(cookieName, cookieValue);
	    }
	    i++;
	}
    }

    /**
     * Remove all cookies from the cookie cache.
     */
    public final void clearCookieCache() {
	COOKIES.clear();
    }
}
