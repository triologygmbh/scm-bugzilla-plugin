/**
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */

package de.triology.scm.plugins.bugzilla;

//~--- non-JDK imports --------------------------------------------------------

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.repository.Changeset;
import sonia.scm.repository.Repository;
import sonia.scm.store.DataStore;
import sonia.scm.store.DataStoreFactory;
import sonia.scm.store.Store;
import sonia.scm.store.StoreFactory;
import sonia.scm.util.AssertUtil;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * 
 * @author Sebastian Sdorra
 */
@Singleton
public class BugzillaGlobalContext {

	/** Field description */
	private static final String NAME = "bugzilla";

	/**
	 * the logger for BugzillaGlobalContext
	 */
	private static final Logger LOG = LoggerFactory.getLogger(BugzillaGlobalContext.class);

	// ~--- fields ---------------------------------------------------------------

	/** Field description */
	private BugzillaGlobalConfiguration configuration;

	/** Field description */
	private final DataStore<BugzillaData> dataStore;

	/** Field description */
	private final Store<BugzillaGlobalConfiguration> store;

	// ~--- constructors ---------------------------------------------------------

	/**
	 * Constructs ...
	 * 
	 * 
	 * @param storeFactory
	 * @param dataStoreFactory
	 */
	@Inject
	public BugzillaGlobalContext(final StoreFactory storeFactory, final DataStoreFactory dataStoreFactory) {
		store = storeFactory.getStore(BugzillaGlobalConfiguration.class, NAME);
		dataStore = dataStoreFactory.getStore(BugzillaData.class, NAME);
		configuration = store.get();

		if (configuration == null) {
			configuration = new BugzillaGlobalConfiguration();
		}
	}

	// ~--- methods --------------------------------------------------------------

	/**
	 * Method description
	 * 
	 * 
	 * @param repository
	 * @param changeset
	 */
	public void markAsHandled(final Repository repository, final Changeset changeset) {
		LOG.debug("mark changeset {} of repository {} as handled", changeset.getId(), repository.getId());

		final BugzillaData data = getData(repository);

		data.getHandledChangesets().add(changeset.getId());
		dataStore.put(repository.getId(), data);
	}

	// ~--- get methods ----------------------------------------------------------

	/**
	 * Method description
	 * 
	 * 
	 * @return
	 */
	public BugzillaGlobalConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * Method description
	 * 
	 * 
	 * @param repository
	 * @param changeset
	 * 
	 * @return
	 */
	public boolean isHandled(final Repository repository, final Changeset changeset) {
		final BugzillaData data = getData(repository);

		return data.getHandledChangesets().contains(changeset.getId());
	}

	// ~--- set methods ----------------------------------------------------------

	/**
	 * Method description
	 * 
	 * 
	 * @param configuration
	 */
	public void setConfiguration(final BugzillaGlobalConfiguration configuration) {
		LOG.debug("store bugzilla configuration");
		AssertUtil.assertIsValid(configuration);
		this.configuration = configuration;
		store.set(configuration);
	}

	// ~--- get methods ----------------------------------------------------------

	/**
	 * Method description
	 * 
	 * 
	 * @param repository
	 * 
	 * @return
	 */
	private BugzillaData getData(final Repository repository) {
		BugzillaData data = dataStore.get(repository.getId());

		if (data == null) {
			data = new BugzillaData();
		}

		return data;
	}
}
