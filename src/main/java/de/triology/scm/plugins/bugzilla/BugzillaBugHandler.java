/**
 * Copyright (c) 2012, TRIOLOGY GmbH
 * 
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */



package de.triology.scm.plugins.bugzilla;

//~--- non-JDK imports --------------------------------------------------------

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.repository.Changeset;

/**
 *
 * @author Sebastian Sdorra
 */
public class BugzillaBugHandler {

    /** the logger for BugzillaBugHandler. */
    private static final Logger LOGGER =
	    LoggerFactory.getLogger(BugzillaBugHandler.class);

    //~--- fields ---------------------------------------------------------------

    /** The BugzillaBugRequest. */
    private final BugzillaBugRequest request;

    /** The TemplateHandler. */
    private final CommentTemplateHandler templateHandler;

    //~--- constructors ---------------------------------------------------------

    /**
     * Constructs the BugzillaBugHandler.
     *
     *
     * @param pTemplateHandler the TemplateHandler.
     * @param pRequest The request.
     */
    public BugzillaBugHandler(final CommentTemplateHandler pTemplateHandler, final BugzillaBugRequest pRequest) {
	templateHandler = pTemplateHandler;
	request = pRequest;
    }

    //~--- methods --------------------------------------------------------------

    /**
     * Check for auto close keywords in the changeset description and handle them.
     *
     * @param bugId current Bug.
     * @param changeset current changeset.
     */
    public final void handleBug(final int bugId, final Changeset changeset) {
	if (request.getConfiguration().isAutoCloseEnabled()) {
	    if (LOGGER.isTraceEnabled()) {
		LOGGER.trace("check changeset {} for auto-close of issue", changeset.getId(), bugId);
	    }

	    final String autoCloseWord = searchAutoCloseWord(changeset);

	    if (autoCloseWord != null) {
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug("found auto close word {} for issue {}", autoCloseWord, bugId);
		}

		closeIssue(changeset, bugId, autoCloseWord);
	    } else {
		if (LOGGER.isDebugEnabled()) {
		    LOGGER.debug("found no auto close word");
		}

		updateBug(changeset, bugId);
	    }
	} else {
	    updateBug(changeset, bugId);
	}
    }

    /**
     * Auto close the given bug.
     *
     * @param changeset The current changeset.
     * @param bugId The current bug.
     * @param autoCloseWord Keywords for auto close.
     */
    private void closeIssue(final Changeset changeset, final int bugId, final String autoCloseWord) {
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("try to close issue {} because of changeset {}", bugId, changeset.getId());
	}

	try {
	    final BugzillaHandler handler = request.createBugzillaHandler();
	    final String comment = templateHandler.render(
		    CommentTemplate.AUTOCLOSE,
		    request,
		    changeset,
		    autoCloseWord);
	    handler.close(bugId);
	    handler.addComment(bugId, comment);
	} catch (final IOException ex) {
	    LOGGER.error("could not render template", ex);
	} catch (final BugzillaException ex) {
	    LOGGER.error("could not close bugzilla issue", ex);
	}
    }

    /**
     * Search for keyword identifying auto close operations.
     *
     * @param changeset the current changeset.
     * @return the identified close keyword.
     */
    private String searchAutoCloseWord(final Changeset changeset) {
	final String description = changeset.getDescription();
	String autoCloseWord = null;
	final String[] words = description.split("\\s");

	for (final String w : words) {
	    for (String acw : request.getConfiguration().getAutoCloseWords()) {
		acw = acw.trim();

		if (w.equalsIgnoreCase(acw)) {
		    autoCloseWord = w;
		    break;
		}
	    }

	    if (autoCloseWord != null) {
		break;
	    }
	}

	return autoCloseWord;
    }

    /**
     * Submit changes to Bugzilla.
     *
     * @param changeset the current changeset.
     * @param bugId the current bug.
     */
    private void updateBug(final Changeset changeset, final int bugId) {
	if (LOGGER.isDebugEnabled()) {
	    LOGGER.debug("try to update issue {} because of changeset {}", bugId, changeset.getId());
	}

	try {
	    final BugzillaHandler handler = request.createBugzillaHandler();

	    if (!handler.isCommentAlreadyExists(bugId, changeset.getId(), changeset.getDescription())) {
		final String comment = templateHandler.render(CommentTemplate.UPADTE, request, changeset);
		handler.addComment(bugId, comment);
	    } else if (LOGGER.isInfoEnabled()) {
		LOGGER.info("comment for changeset {} already exists at bug {}", changeset.getId(), bugId);
	    }


	} catch (final IOException ex) {
	    LOGGER.error("could not render template", ex);
	} catch (final BugzillaException ex) {
	    LOGGER.error("could not update bugzilla bug", ex);
	}
    }
}
