/**
 * Copyright (c) 2012, TRIOLOGY GmbH
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */
package de.triology.scm.plugins.bugzilla;

import java.text.MessageFormat;
import java.util.regex.Matcher;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.xmlrpc.XmlRpcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.repository.EscapeUtil;
import sonia.scm.util.HttpUtil;

import com.google.common.base.Strings;

/**
 *
 * @author Sebastian Sdorra
 * @author Jan Boerner, TRIOLOGY GmbH
 */
public class XmlRpcBugzillaHandler implements BugzillaHandler {

  /** Default action when a auto close keyword was found. */
  public static final String ACTION_DEFAULT_CLOSE = "FIXED";

  /** the logger for the XmlRpcBugzillaHandler. */
  private static final Logger LOGGER =
      LoggerFactory.getLogger(XmlRpcBugzillaHandler.class);

  //~--- fields ---------------------------------------------------------------

  /** Instance of the BugzillaXmlRpcService object. */
  private final BugzillaXmlRpcService service;

  /** URL to the bugzilla server. */
  private final String bugzillaUrl;




  //~--- constructors ---------------------------------------------------------

  /**
   * Constructs a new XmlRpcBugzillaHandler instance.
   *
   * @param pService The BugzillaXmlRpcService instance.
   */
  public XmlRpcBugzillaHandler(final BugzillaXmlRpcService pService, final String pBugzillaUrl) {
    service = pService;
    bugzillaUrl =  HttpUtil.getUriWithoutEndSeperator(pBugzillaUrl);
  }

  //~--- methods --------------------------------------------------------------

  @Override
  public final void addComment(final int bugId, final String comment) throws BugzillaException {
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("add comment to bug {}", bugId);
    }

    try {
      service.addComment(bugId, prepareComment(bugId, comment));
    } catch (final Exception ex) {
      throw new BugzillaException("add comment failed", ex);
    }
  }

  @Override
  public final void close(final int bugId) throws BugzillaException {
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("close bug {}", bugId);
    }

    try {
      service.closeBug(bugId, ACTION_DEFAULT_CLOSE);
    } catch (final Exception ex) {
      throw new BugzillaException("close issue failed", ex);
    }
  }

  @Override
  public final void login(final String loginname, final String password) throws BugzillaException {
    LOGGER.debug("Try to login user " + loginname);
    try {
      service.login(loginname, password);
    } catch (final XmlRpcException e) {
      throw new BugzillaException("Login failed.", e);
    }
  }

  @Override
  public final void logout() throws BugzillaException {
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Logout from bugzilla");
    }
    try {
      service.logout();
    } catch (final XmlRpcException e) {
      throw new BugzillaException("Logout failed.", e);
    }
  }


  /**
   * Things that should be done before the comment is sent to the server.
   *
   * @param bugId The id of the bug.
   * @param comment The comment to prepare.
   *
   * @return The prepared comment string.
   */
  public String prepareComment(final int bugId, final String comment) {
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Preparing comment for bug #" + bugId);
    }
    final String body = Strings.nullToEmpty(comment);
    return removeIssueLink(bugId, body);
  }

  /*
   * (non-Javadoc)
   * @see de.triology.scm.plugins.bugzilla.BugzillaHandler#isCommentAlreadyExists(int, java.lang.String[])
   * 
   * TODO check if this feature can be implemented.
   */
  @Override
  public boolean isCommentAlreadyExists(final int bugId, final String... contains) throws BugzillaException {
    return false;
  }

  /**
   * Remove issue self reference link.
   * {@see https://bitbucket.org/sdorra/scm-manager/issue/337/jira-comment-contains-unneccessary-link}.
   *
   * TODO: The preprocessor order on hooks should be fixed in the core.
   *
   *
   * @param bugId
   * @param body
   *
   * @return
   */
  private String removeIssueLink(final int bugId, final String body) {
    String link = MessageFormat.format(BugzillaChangesetPreProcessorFactory.REPLACEMENT_LINK, bugzillaUrl);
    link = link.replaceAll(Matcher.quoteReplacement("$1"), String.valueOf(bugId));

    final String[] linkParts = link.split("\\$0");
    String result = body.replace(linkParts[0], "");
    result = result.replace(linkParts[1], "");
    result = result.replace(StringEscapeUtils.escapeHtml(linkParts[0]), "");
    result = result.replace(StringEscapeUtils.escapeHtml(linkParts[1]), "");
    if (LOGGER.isTraceEnabled()) {
      LOGGER.trace("Replaced all occurences of '" + link + "' in comment.");
      LOGGER.trace("Replaced all occurences of '" + EscapeUtil.escape(link) + "' in comment.");
      LOGGER.trace("OLD: " + body + "\nNEW: " + result);
    }
    return result;
  }

}
