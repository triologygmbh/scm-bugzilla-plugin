/**
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */

package de.triology.scm.plugins.bugzilla;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.security.Role;

import com.google.inject.Inject;

/**
 * 
 * @author Sebastian Sdorra
 */
@Path("plugins/bugzilla")
public class BugzillaGlobalConfigurationResource {

  /**
   * the logger for BugzillaGlobalConfigurationResource.
   */
  private static final Logger LOG = LoggerFactory.getLogger(BugzillaGlobalConfigurationResource.class);

  // ~--- fields ---------------------------------------------------------------

  /** The global context. */
  private final BugzillaGlobalContext context;

  // ~--- constructors ---------------------------------------------------------

  /**
   * Constructs ...
   * 
   * @param context BugzillaGlobalContext
   */
  @Inject
  public BugzillaGlobalConfigurationResource(final BugzillaGlobalContext context) {
    if (!SecurityUtils.getSubject().hasRole(Role.ADMIN)) {
      LOG.warn("user has not enough privileges to configure bugzilla");

      throw new WebApplicationException(Status.FORBIDDEN);
    }

    this.context = context;
  }

  // ~--- methods --------------------------------------------------------------

  /**
   * Stores new values in the global configuration when an POST request arrives.
   * 
   * 
   * @param updatedConfig BugzillaGlobalConfiguration
   * 
   * @return Response
   */
  @POST
  @Path("global-config")
  @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public final Response updateConfiguration(final BugzillaGlobalConfiguration updatedConfig) {

    context.setConfiguration(updatedConfig);

    return Response.ok().build();
  }

  /**
   * Testing the connection to Bugzilla.
   * 
   * @param configuration BugzillaConfiguration
   * @throws Exception
   */
  @POST
  @Path("test")
  @Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
  public final String testLogin(final BugzillaGlobalConfiguration configuration) throws Exception {

    final BugzillaHandlerFactory factory = new XmlRpcBugzillaHandlerFactory();
    String testResult = "failed";
    try {
      final BugzillaHandler handler = factory.createBugzillaHandler(configuration.getUrl(),
          configuration.getUsername(), configuration.getPassword());
      handler.logout();
      testResult = "success";
    } catch (final BugzillaConnectException e) {
      LOG.error("Login to Bugzilla server failed.", e);
    } catch (final BugzillaException e1) {
      LOG.error(e1.getMessage(), e1);
    }
    return testResult;
  }

  // ~--- get methods ----------------------------------------------------------

  /**
   * getConfiguration.
   * 
   * @return Response
   */
  @GET
  @Path("global-config")
  @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public final Response getConfiguration() {
    return Response.ok(context.getConfiguration()).build();
  }
}
