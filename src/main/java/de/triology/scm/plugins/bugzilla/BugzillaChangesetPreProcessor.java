/**
 * Copyright (c) 2012, TRIOLOGY GmbH
 * 
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */
package de.triology.scm.plugins.bugzilla;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.repository.Changeset;
import sonia.scm.repository.ChangesetPreProcessor;
import sonia.scm.util.Util;

/**
 * This class is called when the user opens the commits overview of the SCM Manager GUI. It parses the changeset
 * messages and replaces bug numbers with links to the configured Bugzilla server.
 * @author Sebastian Sdorra
 * @author jboerner
 */
public class BugzillaChangesetPreProcessor implements ChangesetPreProcessor {

    /** Make use of the logging framework. */
    private static final Logger LOGGER = LoggerFactory.getLogger(BugzillaChangesetPreProcessor.class);

    //~--- fields ---------------------------------------------------------------

    /** The Bugzilla bug handler. */
    private BugzillaBugHandler bugHandler;

    /** Pattern to replace keywords with. */
    private final String keyReplacementPattern;

    /** Pattern to identify a bug number. */
    private final Pattern bugKeyPattern;

    /**
     * Constructs a BugzillaChangesetPreProcessor.
     *
     * @param pKeyReplacementPattern Pattern to replace keywords with.
     * @param pBugKeyPattern Pattern to identify a bug number.
     */
    public BugzillaChangesetPreProcessor(final String pKeyReplacementPattern, final Pattern pBugKeyPattern) {
	LOGGER.trace("Bugzilla changeset pre processer instantiated with patterns: " + pKeyReplacementPattern + ", "
		+ pBugKeyPattern);
	keyReplacementPattern = pKeyReplacementPattern;
	bugKeyPattern = pBugKeyPattern;
    }

    //~--- methods --------------------------------------------------------------

    /**
     * Parse the given change set message for bug numbers. If found, it will be replaces by the configured pattern.
     * If a bugHandler is defined, it is bypassed to its handle method.
     *
     * @param changeset the current changeset.
     */
    @Override
    public final void process(final Changeset changeset) {
	LOGGER.trace("Processing the changeset pre processor.");
	String description = changeset.getDescription();

	if (Util.isNotEmpty(description)) {
	    final StringBuffer sb = new StringBuffer();
	    final Matcher m = bugKeyPattern.matcher(description);
	    while (m.find()) {
		m.appendReplacement(sb, keyReplacementPattern);

		if (null != bugHandler) {
		    bugHandler.handleBug(Integer.parseInt(m.group(1)), changeset);
		}
	    }

	    m.appendTail(sb);
	    description = sb.toString();
	} else {
	    LOGGER.debug("No pre processing of changeset description cause it's empty.");
	}
	changeset.setDescription(description);
    }

    //~--- set methods ----------------------------------------------------------

    /**
     * @param pBugHandler The BugzillaBugHandler to set.
     */
    public final void setBugzillaBugHandler(final BugzillaBugHandler pBugHandler) {
	bugHandler = pBugHandler;
    }

    /**
     * @return The BugzillaBugHanlder.
     */
    public final Object getBugzillaBugHandler() {
	return bugHandler;
    }
}
