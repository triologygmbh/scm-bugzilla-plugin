/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */

package de.triology.scm.plugins.bugzilla;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Configuration settings of the global configuration.
 * 
 * @author Sebastian Sdorra
 * @author Jan Boerner, TRIOLOGY GmbH
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "bugzilla-configuration")
public class BugzillaGlobalConfiguration extends BugzillaConfiguration {

  // ~--- fields ---------------------------------------------------------------

  /** Field description. */
  @XmlElement(name = "disable-repository-configuration")
  private boolean disableRepositoryConfiguration = false;

  /**
   * @return is the repository configuration disabled?
   */
  public final boolean isDisableRepositoryConfiguration() {
    return disableRepositoryConfiguration;
  }

  @Override
  public final String toString() {
    final String strUrl = "\n Url:            " + getUrl();
    final String disRepoConf = "\n DisableRepositoryConfiguration:      " + isDisableRepositoryConfiguration();
    final String strUpdateIssuesEnabled = "\n UpdateIssuesEnabled:        " + isUpdateIssuesEnabled();
    final String strPrefix = "\n BugPrefix:           " + getBugPrefix();
    final String strCaseSensitve = "\n CaseSensitive:         " + isCaseSensitive();
    final String strAutoClose = "\n AutoCloseEnabled:         " + isAutoCloseEnabled();
    final String strAutoCloseWords = "\n AutoCloseWords:          " + getAutoCloseWords();
    final String strUseLogin = "\n Use loginname?               " + isUseLoginname();
    final String strUsername = "\n Username:          " + getUsername();
    return strUrl + disRepoConf + strUpdateIssuesEnabled + strPrefix + strCaseSensitve + strAutoClose
        + strAutoCloseWords + strUseLogin + strUsername;
  }
}
