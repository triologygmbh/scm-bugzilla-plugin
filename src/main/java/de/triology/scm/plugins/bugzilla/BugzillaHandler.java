/**
 * Copyright (c) 2012, TRIOLOGY GmbH
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */



package de.triology.scm.plugins.bugzilla;

/**
 * This interface define methods to handle the communication between the plug in and the Bugzilla server.
 * @author Sebastian Sdorra
 * @author jboerner
 */
public interface BugzillaHandler {

    /**
     * Adds a comment to the Bugzilla ticket identified by its id.
     * @param bugId The Bugzilla bug id.
     * @param comment The comment to add.
     *
     * @throws BugzillaException If the execution of the add command fails.
     */
    void addComment(int bugId, String comment) throws BugzillaException;

    /**
     * Mark a Bugzilla bug as closed.
     * @param bugId The Bugzilla bug id.
     * @throws BugzillaException If the execution of the close command fails.
     */
    void close(int bugId) throws BugzillaException;

    /**
     * Log out the current logged in user from the Bugzilla server.
     *
     * @throws BugzillaException If the execution of the logout command fails.
     */
    void logout() throws BugzillaException;

    /**
     * Perform a login request to the Bugzilla server.
     * @param loginname The users eMail-Address as login name.
     * @param password The users password.
     * @throws BugzillaException If execution of the login command fails.
     */
    void login(String loginname, String password) throws BugzillaException;


    /**
     * Check if the provided comment already exists for the given bug number.
     *
     * @param bugId The bug id.
     * @param contains the comment to check if already existing.
     *
     * @return <code>true</code>, if already existing, <code>false</code> otherwise.
     *
     * @throws BugzillaException if an error occurred while accessing the Bugzilla server.
     */
    public boolean isCommentAlreadyExists(int bugId, String... contains) throws BugzillaException;
}
