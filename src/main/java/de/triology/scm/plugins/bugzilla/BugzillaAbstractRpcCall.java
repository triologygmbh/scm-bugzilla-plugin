package de.triology.scm.plugins.bugzilla;

import com.google.common.base.Strings;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcTransport;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The abstract RPC Call class handles RPC Calls to the Bugzilla RPC interface.
 * 
 * @author jboerner
 */
public class BugzillaAbstractRpcCall {

    /** Make usage of the logging framework. */
    private static final Logger LOGGER = LoggerFactory.getLogger(BugzillaAbstractRpcCall.class);

    /** The client instance. */
    private static XmlRpcClient client = null;

    /** Parameters map. */
    private Map<String, Object> parameters = new HashMap<String, Object>();
    /** API command. */
    private String command;

    /** Default path to Bugzilla XML-RPC interface. */
    private static final String BZ_PATH = "https://localhost/bugzilla/xmlrpc.cgi";
    /** Path to Bugzilla XML-RPC interface. */
    private URL bzPath;
    /** Bugzilla Server version. */
    private String bzVersion;
    
    /** 
     * login token for bugzilla >= 4.4.3 
     * @since 1.4
     **/
    private String loginToken;

    /**
     * Creates a new instance of the Bugzilla XML-RPC command executor for a specific command.
     * @param pCommand A remote method associated with this instance of RPC call executor
     * @param path The path to the Bugzilla API.
     */
    public BugzillaAbstractRpcCall(final String pCommand, final URL path) {
	if (null != path) {
	    setBugzillaPath(path);
	}
	synchronized (this) {
	    command = pCommand;
	    initialize();
	}

    }

    /**
     * Do the initializing stuff.
     */
    private void initialize() {
	LOGGER.trace("Initializing the RPC-Call");
	if (client == null) { // assure the initialization is done only once
	    client = new XmlRpcClient();
	    final XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
	    if (null == bzPath) {
		try {
		    bzPath = new URL(BZ_PATH);
		} catch (final MalformedURLException ex) {
		    LOGGER.error("Malformed Bugzilla URL", ex);
		}
	    }
	    config.setServerURL(bzPath);
	    final XmlRpcTransportFactory factory = new XmlRpcTransportFactory() {

		@Override
		public XmlRpcTransport getTransport() {
		    return new XmlRpcBugzillaHttpTransport(client);
		}
	    };
	    client.setTransportFactory(factory);
	    client.setConfig(config);
	    if (null == bzVersion) {
		loadBugzillaVersion();
	    }
	}
    }

    /**
     * Get the parameters of this call, that were set using setParameter method.
     * @return Array with a parameter hashmap
     */
    protected final Object[] getParameters() {
	return new Object[] {parameters};
    }

    /**
     * Set parameter to a given value.
     * @param name Name of the parameter to be set
     * @param value A value of the parameter to be set
     * @return Previous value of the parameter, if it was set already.
     */
    public final Object setParameter(final String name, final Object value) {
	return parameters.put(name, value);
    }
    
    /**
     * Set login token for bugzilla versions >= 4.4.3.
     * 
     * @param loginToken login token
     */
    protected final void setLoginToken(final String loginToken) {
    this.loginToken = loginToken;
    }

    /**
     * Executes the XML-RPC call to Bugzilla instance and returns a map with result.
     * @return A map with response
     * @throws XmlRpcException when not able to execute to command.
     */
    public final Map<?, ?> execute() throws XmlRpcException {
	LOGGER.debug("Executing command: " + command);
    if (!Strings.isNullOrEmpty(loginToken)) {
        setParameter("token", loginToken);
    }
	final Object response = client.execute(command, getParameters());
	if (response instanceof Map) {
	    return (Map<?, ?>) response;
	}
	return null;
    }

    /**
     * @return the Bugzilla path.
     */
    public final URL getBugzillaPath() {
	return bzPath;
    }

    /**
     * @param bugzillaPath the bzPath to set.
     */
    public final void setBugzillaPath(final URL bugzillaPath) {
	bzPath = bugzillaPath;
    }

    /**
     * @return the command
     */
    public final String getCommand() {
	return command;
    }

    /**
     * @param pCommand the command to set
     */
    public final void setCommand(final String pCommand) {
	command = pCommand;
    }

    /**
     * @return the XmlRpcClient object.
     */
    protected final XmlRpcClient getClient() {
	return client;
    }

    /**
     * Remove all parameters.
     */
    protected final void clearParameters() {
	parameters = new HashMap<String, Object>();
    }

    /**
     * Load the current Bugzilla version.
     */
    public final void loadBugzillaVersion() {

    }

}
