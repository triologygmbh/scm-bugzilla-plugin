/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * Copyright (c) 2010, Sebastian Sdorra
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */



package de.triology.scm.plugins.bugzilla;

import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.plugin.ext.Extension;
import sonia.scm.repository.Changeset;
import sonia.scm.repository.PostReceiveRepositoryHook;
import sonia.scm.repository.Repository;
import sonia.scm.repository.RepositoryHookEvent;
import sonia.scm.repository.RepositoryHookType;
import sonia.scm.util.IOUtil;
import sonia.scm.util.Util;

import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Entry Point for adding comments to a bug or perform an auto close action.
 * @author Sebastian Sdorra
 * @author Jan Boerner, TRIOLOGY GmbH
 */
@Extension
public class BugzillaBugPostReceiveHook extends PostReceiveRepositoryHook {

    /** A collection of POST_RECEIVE hook types. */
    public static final Collection<RepositoryHookType> TYPES = Arrays.asList(RepositoryHookType.POST_RECEIVE);

    /** the logger for BugzillaBugPostReceiveHook. */
    private static final Logger LOGGER = LoggerFactory.getLogger(BugzillaBugPostReceiveHook.class);

    //~--- fields ---------------------------------------------------------------

    /** Factory to perform pre processing. */
    private final BugzillaChangesetPreProcessorFactory changesetPreProcessorFactory;

    /** Factory to perform requests. */
    private final BugzillaBugRequestFactory requestFactory;

    /** The template handler. */
    private final Provider<CommentTemplateHandler> templateHandlerProvider;

    /** The global Bugzilla context. */
    private final BugzillaGlobalContext context;

    //~--- constructors ---------------------------------------------------------

    /**
     * Constructs a bug post receive hook instance.
     *
     * @param pRequestFactory an instance of a requestFactory to submit changes to a bugzilla server.
     * @param pTemplateHandler used to format the comment output.
     */
    @Inject
    public BugzillaBugPostReceiveHook(final BugzillaGlobalContext pContext,
	    final BugzillaBugRequestFactory pRequestFactory,
	    final Provider<CommentTemplateHandler> pTemplateHandlerProvider) {
	context = pContext;
	requestFactory = pRequestFactory;
	templateHandlerProvider = pTemplateHandlerProvider;
	changesetPreProcessorFactory = new BugzillaChangesetPreProcessorFactory(context);
    }

    //~--- methods --------------------------------------------------------------

    @Override
    public final void onEvent(final RepositoryHookEvent event) {
	final Repository repository = event.getRepository();

	if (repository != null)	{
	    final BugzillaConfiguration configuration = BugzillaConfigurationResolver.resolve(context, repository);

	    if (configuration.isValid()) {
		if (configuration.isUpdateIssuesEnabled()) {
		    handleBugEvent(event, repository, configuration);
		} else if (LOGGER.isTraceEnabled()) {
		    LOGGER.trace("bugzilla auto-close is disabled");
		}
	    } else if (LOGGER.isDebugEnabled()) {
		LOGGER.debug("no valid bugzilla configuration found");
	    }
	} else if (LOGGER.isErrorEnabled()) {
	    LOGGER.error("receive repository hook without repository");
	}
    }

    /**
     * Reads in change sets and process every one of them in the change set pre processor.
     *
     * @param event An event containing the change sets.
     * @param repository the actual repository.
     * @param configuration the configuration object.
     */
    private void handleBugEvent(final RepositoryHookEvent event, final Repository repository,
	    final BugzillaConfiguration configuration) {
	final Collection<Changeset> changesets = event.getChangesets();

	if (Util.isNotEmpty(changesets)) {
	    final BugzillaChangesetPreProcessor bcpp = changesetPreProcessorFactory.createPreProcessor(repository);
	    BugzillaBugRequest request = null;

	    try {
		request = requestFactory.createRequest(configuration, repository);
		bcpp.setBugzillaBugHandler(new BugzillaBugHandler(templateHandlerProvider.get(), request));

		for (final Changeset c : changesets) {
		    bcpp.process(c);
		}
	    } finally {
		IOUtil.close(request);
	    }
	} else if (LOGGER.isWarnEnabled()) {
	    LOGGER.warn("receive repository hook without changesets");
	}
    }
}
