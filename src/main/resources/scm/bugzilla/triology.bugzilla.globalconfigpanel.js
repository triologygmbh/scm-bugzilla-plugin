/*
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */
Ext.ns("Triology.bugzilla");

Triology.bugzilla.GlobalConfigPanel = Ext.extend(Sonia.config.ConfigForm, {
  initComponent : function() {

    var config = {
      title : Triology.bugzilla.I18n.titleText,
      items : [ {
        xtype : 'textfield',
        fieldLabel : Triology.bugzilla.I18n.urlText,
        name : 'url',
        vtype : 'url',
        allowBlank : true,
        helpText : Triology.bugzilla.I18n.urlHelpText
      }, {
        xtype : 'checkbox',
        fieldLabel : Triology.bugzilla.I18n.repositoryConfigurationText,
        name : 'disable-repository-configuration',
        inputValue : 'true',
        helpText : Triology.bugzilla.I18n.repositoryConfigurationHelpText
      }, {
        id : 'updateIssues',
        name : 'update-issues',
        xtype : 'checkbox',
        inputValue : 'true',
        fieldLabel : Triology.bugzilla.I18n.updateIssuesText,
        helpText : Triology.bugzilla.I18n.updateIssuesHelpText,
        listeners : {
          check : {
            scope : this,
            fn : this.toggleUpdateIssues
          }
        }

      }, {
        id : 'bugPrefix',
        name : 'bug-prefix',
        xtype : 'textfield',
        fieldLabel : Triology.bugzilla.I18n.bugPrefixText,
        helpText : Triology.bugzilla.I18n.bugPrefixHelpText,
        allowBlank : false,
        validator : function(value) {
          var symbols = '[-!$%^&?,.*()_+`´~=:;\"\'<>/\\\\|{}\\[\\]]';
          if (value.match(symbols)) {
            return Triology.bugzilla.I18n.bugPrefixValidErrorText;
          } else {
            return true;
          }
        }
      }, {
        id : 'prefixCaseSensitive',
        name : 'case-sensitive',
        xtype : 'checkbox',
        fieldLabel : Triology.bugzilla.I18n.bugPrefixCaseSensitiveText,
        helpText : Triology.bugzilla.I18n.bugPrefixCaseSensitiveHelpText,
        inputValue : 'true'
      }, {
        id : 'autoClose',
        name : 'auto-close',
        xtype : 'checkbox',
        inputValue : 'true',
        fieldLabel : Triology.bugzilla.I18n.autoCloseText,
        helpText : Triology.bugzilla.I18n.autoCloseHelpText,
        listeners : {
          check : {
            scope : this,
            fn : this.toggleAutoClose
          }
        }
      }, {
        id : 'autoCloseWords',
        name : 'auto-close-words',
        xtype : 'textfield',
        fieldLabel : Triology.bugzilla.I18n.autoCloseWordsText,
        helpText : Triology.bugzilla.I18n.autoCloseWordsHelpText,
        value : Triology.bugzilla.I18n.autoCloseDefaultValues
      }, {
        id : 'useLoginname',
        name : 'use-loginname',
        xtype : 'checkbox',
        fieldLabel : Triology.bugzilla.I18n.useLoginnameInsteadEmailText,
        helpText : Triology.bugzilla.I18n.useLoginnameInsteadEmailHelpText,
        inputValue : 'true',
        listeners : {
          check: {
            scope: this,
            fn : this.toggleLoginnameValidator
          }
        }
      }, {
        id : 'username',
        name : 'username',
        vtype : 'email',
        xtype : 'textfield',
        fieldLabel : Triology.bugzilla.I18n.usernameText,
        helpText : Triology.bugzilla.I18n.usernameHelpText
      }, {
        id : 'password',
        name : 'password',
        xtype : 'textfield',
        fieldLabel : Triology.bugzilla.I18n.passwordText,
        inputType : 'password',
        helpText : Triology.bugzilla.I18n.passwordHelpText,
        validator : function(value) {
          var userName = Ext.getCmp('username').getValue();
          if (!Ext.isEmpty(userName) && Ext.isEmpty(value)) {
            return Triology.bugzilla.I18n.passwordValidErrorText;
          } else {
            return true;
          }
        }
      }, {
        xtype : 'button',
        text : Triology.bugzilla.I18n.testConfigurationText,
        fieldLabel : Triology.bugzilla.I18n.testConfigurationText,
        helpText: Triology.bugzilla.I18n.testConfigurationHelpText,
        scope : this,
        handler : function() {
          this.testBugzillaLogin();
        }
      } ]
    };

    Ext.apply(this, Ext.apply(this.initialConfig, config));
    Triology.bugzilla.GlobalConfigPanel.superclass.initComponent.apply(this, arguments);
  },

  toggleUpdateIssues : function(checkbox) {
    var autoclose = Ext.getCmp('autoClose');
    var cmps = [ autoclose, Ext.getCmp('autoCloseWords'), Ext.getCmp('username'), Ext.getCmp('password'),
                 Ext.getCmp('useLoginname')];

    Triology.bugzilla.toggleFields(cmps, checkbox);

    if (!checkbox.getValue()) {
      autoclose.setValue(false);
    }
    this.toggleAutoClose(autoclose);
  },

  toggleAutoClose : function(checkbox) {
    var autoCloseWords = [ Ext.getCmp('autoCloseWords') ];
    Triology.bugzilla.toggleFields(autoCloseWords, checkbox);
  },

  testBugzillaLogin : function() {
    var values = this.getForm().getValues();
    this.el.mask(this.submitText);
    Ext.Ajax.request({
      url : restUrl + 'plugins/bugzilla/test',
      method : 'POST',
      jsonData : values,
      scope : this,
      disableCaching : true,
      success : function(response) {
        this.el.unmask();
        var t;
        var msg;
        var icn;
        if ('success' == response.responseText) {
          t = Triology.bugzilla.I18n.testConfigurationSuccessBoxTitle;
          msg = Triology.bugzilla.I18n.testConfigurationSuccessText;
          icn = Ext.MessageBox.INFO;
        } else {
          t = Triology.bugzilla.I18n.errorBoxTitle;
          msg = Triology.bugzilla.I18n.errorOnTestText;
          icn = Ext.MessageBox.ERROR;
        }
        Ext.MessageBox.show({
          title : t,
          msg : msg,
          buttons : Ext.MessageBox.OK,
          icon : icn
        });
      },
      failure : function() {
        this.el.unmask();
        Ext.MessageBox.show({
          title : Triology.bugzilla.I18n.errorBoxTitle,
          msg : Triology.bugzilla.I18n.errorOnTestText,
          buttons : Ext.MessageBox.OK,
          icon : Ext.MessageBox.ERROR
        });
      }
    });
  },
  
  toggleLoginnameValidator : function() {
    var checked = Ext.getCmp('useLoginname').getValue();
    var cmp = Ext.getCmp('username');
    if (checked) {
      cmp.vtype = null;
    } else {
      cmp.vtype = 'email';
    }
    cmp.validate();
  },

  onSubmit : function(values) {
    var form = this.getForm();
    if (form.isValid()) {
      this.el.mask(this.submitText);
      Ext.Ajax.request({
        url : restUrl + 'plugins/bugzilla/global-config.json',
        method : 'POST',
        jsonData : values,
        scope : this,
        disableCaching : true,
        success : function(response) {
          this.el.unmask();
        },
        failure : function() {
          this.el.unmask();
          Ext.MessageBox.show({
            title : Triology.bugzilla.I18n.errorBoxTitle,
            msg : Triology.bugzilla.I18n.errorOnSubmitText,
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.ERROR
          });
        }
      });
    } else {
      // TODO: mask the save button.
      Ext.MessageBox.show({
        title : Triology.bugzilla.I18n.errorBoxTitle,
        msg : Triology.bugzilla.I18n.errorValidSubmitText,
        buttons : Ext.MessageBox.OK,
        icon : Ext.MessageBox.ERROR
      });
    }
  },

  onLoad : function(el) {
    var tid = setTimeout(function() {
      el.mask(this.loadingText);
    }, 100);
    Ext.Ajax.request({
      url : restUrl + 'plugins/bugzilla/global-config.json',
      method : 'GET',
      scope : this,
      disableCaching : true,
      success : function(response) {
        var obj = Ext.decode(response.responseText);
        this.load(obj);

        // toggle fields
        var cmp = Ext.getCmp('updateIssues');
        this.toggleUpdateIssues(cmp);

        clearTimeout(tid);
        el.unmask();
      },
      failure : function() {
        el.unmask();
        clearTimeout(tid);
        Ext.MessageBox.show({
          title : Triology.bugzilla.I18n.errorBoxTitle,
          msg : Triology.bugzilla.I18n.errorOnLoadText,
          buttons : Ext.MessageBox.OK,
          icon : Ext.MessageBox.ERROR
        });
      }
    });
  }

});

// register xtype
Ext.reg("bugzillaGlobalConfigPanel", Triology.bugzilla.GlobalConfigPanel);

// register config panel
registerGeneralConfigPanel({
  id : 'bugzillaGlobalConfigPanel',
  xtype : 'bugzillaGlobalConfigPanel'
});
