/*
 * Copyright (c) 2013, Triology GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */
Ext.ns('Triology.bugzilla');

Triology.bugzilla.RepositoryConfigPanel = Ext.extend(Sonia.repository.PropertiesFormPanel, {

  initComponent : function() {
    var config = {
      title : Triology.bugzilla.I18n.formTitleText,
      items : [ {
        name : 'bugzillaUrl',
        fieldLabel : Triology.bugzilla.I18n.urlText,
        property : 'bugzilla.url',
        vtype : 'url',
        helpText : Triology.bugzilla.I18n.urlHelpText
      }, {
        id : 'bugzillaUpdateIssues',
        name : 'bugzillaUpdateIssues',
        xtype : 'checkbox',
        fieldLabel : Triology.bugzilla.I18n.updateIssuesText,
        property : 'bugzilla.update-issues',
        helpText : Triology.bugzilla.I18n.updateIssuesHelpText,
        listeners : {
          check : {
            fn : this.toggleUpdateIssues,
            scope : this
          }
        }
      }, {
        id : 'bugzillaBugPrefix',
        name : 'bugzillaBugPrefix',
        xtype : 'textfield',
        fieldLabel : Triology.bugzilla.I18n.bugPrefixText,
        property : 'bugzilla.bug-prefix',
        helpText : Triology.bugzilla.I18n.bugPrefixHelpText,
        allowBlank : false,
        validator : function(value) {
          var symbols = '[-!$%^&?,.*()_+`´~=:;\"\'<>/\\\\|{}\\[\\]]';
          if (value.match(symbols)) {
            return Triology.bugzilla.I18n.bugPrefixValidErrorText;
          } else {
            return true;
          }
        }
      }, {
        id : 'bugzillaPrefixCaseSensitive',
        name : 'bugzilla-case-sensitive',
        xtype : 'checkbox',
        fieldLabel : Triology.bugzilla.I18n.bugPrefixCaseSensitiveText,
        helpText : Triology.bugzilla.I18n.bugPrefixCaseSensitiveHelpText,
        property : 'bugzilla.case-sensitive',
        inputValue : 'true'
      }, {
        id : 'bugzillaAutoClose',
        name : 'bugzillaAutoClose',
        xtype : 'checkbox',
        fieldLabel : Triology.bugzilla.I18n.autoCloseText,
        property : 'bugzilla.auto-close',
        helpText : Triology.bugzilla.I18n.autoCloseHelpText,
        listeners : {
          check : {
            fn : this.toggleAutoClose,
            scope : this
          }
        }
      }, {
        id : 'bugzillaAutoCloseWords',
        name : 'bugzillaAutoCloseWords',
        fieldLabel : Triology.bugzilla.I18n.autoCloseWordsText,
        property : 'bugzilla.auto-close-words',
        helpText : Triology.bugzilla.I18n.autoCloseWordsHelpText,
        value : Triology.bugzilla.I18n.autoCloseDefaultValues
      }, {
        id : 'bugzillaUseLoginname',
        name : 'bugzillaUseLoginname',
        xtype : 'checkbox',
        fieldLabel : Triology.bugzilla.I18n.useLoginnameInsteadEmailText,
        helpText : Triology.bugzilla.I18n.useLoginnameInsteadEmailHelpText,
        property : 'bugzilla.use-loginname',
        inputValue : 'true',
        listeners : {
          check : {
            scope: this,
            fn : this.toggleLoginnameValidator
          }
        }
      }, {
        id : 'bugzillaUsername',
        name : 'bugzillaUsername',
        vtype : 'email',
        fieldLabel : Triology.bugzilla.I18n.usernameText,
        property : 'bugzilla.username',
        helpText : Triology.bugzilla.I18n.usernameHelpText
      }, {
        id : 'bugzillaPassword',
        name : 'bugzillaPassword',
        fieldLabel : Triology.bugzilla.I18n.passwordText,
        property : 'bugzilla.password',
        inputType : 'password',
        helpText : Triology.bugzilla.I18n.passwordHelpText,
        validator : function(value) {
          var userName = Ext.getCmp('bugzillaUsername').getValue();
          if (!Ext.isEmpty(userName) && Ext.isEmpty(value)) {
            return Triology.bugzilla.I18n.passwordValidErrorText;
          } else {
            return true;
          }
        }
      } ]
    };

    Ext.apply(this, Ext.apply(this.initialConfig, config));
    Triology.bugzilla.RepositoryConfigPanel.superclass.initComponent.apply(this, arguments);
  },

  loadExtraProperties : function(item) {
    var cmp = Ext.getCmp('bugzillaUpdateIssues');
    this.toggleUpdateIssues(cmp);
  },

  toggleUpdateIssues : function(checkbox) {
    var autoclose = Ext.getCmp('bugzillaAutoClose');
    var cmps = [ autoclose, Ext.getCmp('bugzillaAutoCloseWords'), Ext.getCmp('bugzillaUsername'),
        Ext.getCmp('bugzillaPassword'), Ext.getCmp('bugzillaUseLoginname') ];

    Triology.bugzilla.toggleFields(cmps, checkbox);

    if (!checkbox.getValue()) {
      autoclose.setValue(false);
    }
    this.toggleAutoClose(autoclose);
  },

  toggleAutoClose : function(checkbox) {
    var autoCloseWords = [ Ext.getCmp('bugzillaAutoCloseWords') ];
    Triology.bugzilla.toggleFields(autoCloseWords, checkbox);
  },
  
  toggleLoginnameValidator : function(box) {
    var cmp = Ext.getCmp('bugzillaUsername');
    if (null != box && null != cmp) {
      if (box.getValue()) {
        cmp.vtype = null;
      } else {
        cmp.vtype = 'email';
      }
      cmp.validate();
    }
  }
});

// register xtype
Ext.reg('bugzillaRepositoryConfigPanel', Triology.bugzilla.RepositoryConfigPanel);

// register panel
Sonia.repository.openListeners.push(function(repository, panels) {
  if (Sonia.repository.isOwner(repository)) {
    panels.push({
      xtype : 'bugzillaRepositoryConfigPanel',
      item : repository
    });
  }
});
