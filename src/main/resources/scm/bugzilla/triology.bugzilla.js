/* *
 * Copyright (c) 2012, TRIOLOGY GmbH
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://bitbucket.org/sdorra/scm-manager
 *
 */

Ext.ns('Triology.bugzilla');

Triology.bugzilla.I18n = {

  titleText : 'Bugzilla Configuration',
  formTitleText : 'Bugzilla',

  bugPrefixText : 'Bug number prefix',
  bugPrefixHelpText : 'A prefix for bug numbers to identify a number as bug number (e.g. \'#\' or\n\'Bug\' will match \'#123\' or \'Bug123\').',
  bugPrefixValidErrorText : 'This charachters are not valid: \n -!$%^&?,.*()_+`´~=:;\"\'<>/\|{}[]',

  bugPrefixCaseSensitiveText : 'Case sensitive',
  bugPrefixCaseSensitiveHelpText : 'The prefix should be matched case sensitive (e.g. \'bug\' will not match \'BUG\').',

  repositoryConfigurationText : 'Do not allow repository configuration',
  repositoryConfigurationHelpText : 'Do not allow repository owners to configure bugzilla instances. \n\
    You have to restart your application server after changing this value.',

  urlText : 'Url',
  urlHelpText : 'Url of Bugzilla installation (with contextpath).',

  autoCloseText : 'Enable Auto-Close',
  autoCloseHelpText : 'Enables the auto close function. SCM-Manager searches for \n\
                      issue keys and auto close words in commit messages. If \n\
                      both found in a message SCM-Manager closes the issue in \n\
                      the bugzilla server.',
  autoCloseDefaultValues : 'fixed, fix, closed, close, resolved, resolve',

  updateIssuesText : 'Update Bugzilla Bugs',
  updateIssuesHelpText : 'Enable the automatic update function. SCM-Manager searches for \n\
                         bug numbers in commit messages. If a bug id is found SCM-Manager\n\
                         updates the bug with a comment.',

  autoCloseWordsText : 'Auto-Close Words',
  autoCloseWordsHelpText : 'Comma separated list of words to enable the auto close function. \n\
                           Each commit message of a changeset is being searched for these words.',

  usernameText : 'Username',
  usernameHelpText : 'Bugzilla login name for connection. Leave this field empty to create the connection\n\
                     with the credentials of the pushing user.',

  useLoginnameInsteadEmailText : 'Use Login instead E-Mail',
  useLoginnameInsteadEmailHelpText : 'Use the login name of the user instead of the E-Mail address.\n\
                                        This is useful if you don\'t use the default authentication provider.',
  passwordText : 'Password',
  passwordHelpText : 'Bugzilla password for connection.',
  passwordValidErrorText : 'Password should not be empty.',

  testConfigurationText : 'Test Bugzilla Login',
  testConfigurationHelpText : 'Tries to connect to the configured Bugzilla with the given username and password above.',
  testConfigurationSuccessBoxTitle : 'Success',
  testConfigurationSuccessText : 'Login was successful.',

  // errors
  errorBoxTitle : 'Error',
  errorOnSubmitText : 'Error during config submit.',
  errorValidSubmitText : 'Make sure that your entry is valid.',
  errorOnLoadText : 'Error during config load.',
  errorOnTestText : 'Login failed.'
};

// German translation

if ('de' === i18n.country) {
  Triology.bugzilla.I18n = {

    titleText : 'Bugzilla Einstellungen',
    formTitleText : 'Bugzilla',

    bugPrefixText : 'Bug-Nummer-Prefix',
    bugPrefixHelpText : 'Ein Prefix, um Nummern als Bug-Nummern identifiziert (z.B. \'#\' oder\n\'Bug\' erkennt \'#123\' oder \'Bug123\').',
    bugPrefixValidErrorText : 'Folgende Zeichen sind nicht erlaubt: \n -!$%^&?,.*()_+`´~=:;\"\'<>/\|{}[]',

    bugPrefixCaseSensitiveText : 'Groß- /Kleinschreibung',
    bugPrefixCaseSensitiveHelpText : 'Der Prefix soll zwischen Groß- und Kleinschreibung unterscheiden (z.B. \'bug\' erkennt nicht \'BUG\').',

    repositoryConfigurationText : 'Repository-Einstellungen verbieten',
    repositoryConfigurationHelpText : 'Verhindert, dass Repository-Besitzer eigene Bugzilla-Einstellungen vornehmen können. \n\
		    Der SCM-Manager muss nach einer Änderung neu gestartet werden.',

    urlText : 'Url',
    urlHelpText : 'Url der Bugzilla-Installation.',

    autoCloseText : 'Aktiviere Auto-Close',
    autoCloseHelpText : 'Aktiviert die Auto-Close-Funktion. Der SCM-Manager sucht nach \n\
		                      Bug-Nummern und Auto-Close-Schlüsselwörtern in Commit-Nachrichten. Wenn \n\
		                      beides gefunden wurde, wird der entsprechende Bug im Bugzilla als in \n\
		                      erledigt markiert.',
    autoCloseDefaultValues : 'fixed, fix, closed, close, resolved, resolve',

    updateIssuesText : 'Bugs bearbeiten',
    updateIssuesHelpText : 'Aktiviert die automatische Bearbeitungsfunktion für Bugzilla-Bugs. Der SCM-Manager\n\
		                         sucht nach Bug-Nummern in Commit-Nachrichten. Wenn dies erfolgreich ist, werden\n\
		                         die entsprechenden Bugs im Bugzilla um Kommentare ergänzt.',

    autoCloseWordsText : 'Auto-Close-Schlüsselwörter',
    autoCloseWordsHelpText : 'Komma-getrente Liste an Schlüsselwörtern, die einen Auto-Close aufrufen sollen. \n\
		                           Jede Commit-Nachricht wird nach diesen Wörtern durchsucht.',

    useLoginnameInsteadEmailText : 'Login statt E-Mail',
    useLoginnameInsteadEmailHelpText : 'Benutze für die Anmeldung am Bugzilla den Benutzernamen anstatt der E-Mail-Adresse. \n\
                                        Dies ist sinnvoll, wenn ein externer Authentifizierungsdienst verwendet wird.',
	usernameText : 'Benutzername',
    usernameHelpText : 'Benutzername des Bugzilla-Benutzers. Wenn dieses Feld leer gelassen wird, wird versucht\n\
		                     sich mit den Benutzerdaten des aktuell angemeldeten Benutzers am Bugzilla anzumelden.',

    passwordText : 'Passwort',
    passwordHelpText : 'Passwort des Bugzilla-Benutzers.',
    passwordValidErrorText : 'Das Passwortfeld darf nicht leer sein!',

    testConfigurationText : 'Anmeldung am Bugzilla testen',
    testConfigurationHelpText : 'Started den Versuch, sich am konfigurierten Bugzilla anzumelden. Dazu werden die\n\
                                 o.a. Login-Daten verwendet.',
    testConfigurationSuccessBoxTitle : 'Erfolgreich',
    testConfigurationSuccessText : 'Der Anmeldeversuch war erfolgreich.',

    // errors
    errorBoxTitle : 'Fehler',
    errorOnSubmitText : 'Ein Fehler ist bei der Übermittlung der Daten aufgetreten.',
    errorValidSubmitText : 'Bitte Eingaben auf Korrektheit überprüfen.',
    errorOnLoadText : 'Fehler beim Laden der Konfigurations-Werte.',
    errorOnTestText : 'Anmeldeversuch fehlgeschlagen!'
  };
}

Triology.bugzilla.toggleFields = function(cmps, scope) {
  Ext.each(cmps, function(cmp) {

    var checked = this.getValue();
    // If cmp is a checkbox, use enable/disable.
    if (cmp.getXType() === "checkbox") {
      if (!checked) {
        cmp.disable();
      } else {
        cmp.enable();
      }
    } else {
      // Add/remove CSS class which indicates disabling.
      if (!checked) {
        if (!cmp.readOnly) {
          cmp.addClass('x-item-disabled');
        }
      } else {
        cmp.removeClass('x-item-disabled');
      }

      cmp.setReadOnly(!checked);
    }
  }, scope);
};
