package de.triology.scm.plugins.bugzilla;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Runs all the defined test cases.
 * @author jboerner
 *
 */
@RunWith(Suite.class)
@SuiteClasses({
    BugzillaChangesetPreProcessorTest.class,
    BugzillaChangestPreProcessorFactoryTest.class,
    XmlRpcBugzillaHandlerFactoryTest.class })
public class AllTests {

}
