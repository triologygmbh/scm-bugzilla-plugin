/**
 * (c) 2012 TRIOLOGY GmbH
 */
package de.triology.scm.plugins.bugzilla;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;

/**
 * @author jboerner
 *
 */
public class BugzillaChangesetPreProcessorTest {
    private final static String LINK = "<a target=\"_blank\" href=\"http://teefix/bugzilla/show_bug.cgi?id=$1\">$0</a>";
    private static final Pattern PATTERN = Pattern.compile("#([0-9]+)");

    private BugzillaChangesetPreProcessor processor;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
	processor = new BugzillaChangesetPreProcessor(LINK,  PATTERN);
    }

    /**
     * Test method for
     * {@link de.triology.scm.plugins.bugzilla.BugzillaChangesetPreProcessor#BugzillaChangesetPreProcessor(
     *   java.lang.String, java.util.regex.Pattern)}.
     */
    @Test
    public final void testBugzillaChangesetPreProcessor() {

	processor = new BugzillaChangesetPreProcessor(LINK, PATTERN);
	assertNotNull(processor);
    }
    //
    //    /**
    //     * Test method for {@link de.triology.scm.plugins.bugzilla.BugzillaChangesetPreProcessor#process(
    //     *   sonia.scm.repository.Changeset)}.
    //     */
    //    @Test
    //    public final void testProcess() {
    //	final Changeset change = new Changeset();
    //	change.setAuthor(new Person("scmadmin"));
    //	final String message = "Changed #1 of 2 to 123 by #24 on 32.";
    //	change.setDescription(message);
    //	change.setId("abc");
    //	processor.process(change);
    //	assertEquals("Changed <a target=\"_blank\" href=\"http://teefix/bugzilla/show_bug.cgi?id=1\">#1</a> of 2 to "
    //		+ "123 by <a target=\"_blank\" href=\"http://teefix/bugzilla/show_bug.cgi?id=24\">#24</a> on 32.",
    //		change.getDescription());
    //
    //	final Repository repository = new Repository();
    //	repository.setProperty("bugzilla.url", "http://teefix/bugzilla");
    //	repository.setProperty("bugzilla.bug-prefix", "#");
    //	repository.setProperty("bugzilla.update-issues", "0");
    //	repository.setProperty("bugzilla.auto-close", "0");
    //	repository.setProperty("bugzilla.auto-close-words", "");
    //	repository.setProperty("bugzilla.auto-close-username-transformer", "");
    //
    //	final BugzillaBugRequest request = new BugzillaBugRequest(new XmlRpcBugzillaHandlerFactory(), "username",
    //		"password", new BugzillaConfiguration(repository), repository);
    //
    //	processor.setBugzillaBugHandler(new BugzillaBugHandler(null, request));
    //	processor.process(change);
    //    }

    /**
     * Test method for {@link de.triology.scm.plugins.bugzilla.BugzillaChangesetPreProcessor#setBugzillaBugHandler(
     *   de.triology.scm.plugins.bugzilla.BugzillaBugHandler)}.
     */
    @Test
    public final void testSetBugzillaBugHandler() {
	final BugzillaBugHandler bug = new BugzillaBugHandler(null, null);
	processor.setBugzillaBugHandler(bug);
	assertNotNull(processor);
	assertSame(processor.getBugzillaBugHandler(), bug);
    }

}
