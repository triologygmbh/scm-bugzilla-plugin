/**
 * (c) 2013, TRIOLOGY GmbH
 */
package de.triology.scm.plugins.bugzilla;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

/**
 * This test case tests the BugzillaAbstractRpcCall class.
 * @author Jan Boerner, TRIOLOGY GmbH
 */
public class BugzillaAbstractRpcCallTest {

    private BugzillaAbstractRpcCall rpc = null;

    @Before
    public void setUp() throws Exception {
	rpc = new BugzillaAbstractRpcCall(null, null);
    }

    /**
     * Test method for {@link de.triology.scm.plugins.bugzilla.BugzillaAbstractRpcCall#BugzillaAbstractRpcCall(java.lang.String, java.net.URL)}.
     */
    @Test
    public final void testBugzillaAbstractRpcCall() {
	assertNotNull(rpc);
    }

    /**
     * Test method for {@link de.triology.scm.plugins.bugzilla.BugzillaAbstractRpcCall#getParameters()}.
     */
    @Test
    public final void testGetParameters() {
	assertNotNull(rpc.getParameters());
    }

    //    /**
    //     * Test method for {@link de.triology.scm.plugins.bugzilla.BugzillaAbstractRpcCall#setParameter(java.lang.String,
    //     * java.lang.Object)}.
    //     */
    //    @Test
    //    public final void testSetParameter() {
    //	final int paramsBefore = rpc.getParameters().length;
    //	rpc.setParameter("test", "test");
    //	final int paramsAfter = rpc.getParameters().length;
    //	assertNotEquals(paramsBefore, paramsAfter);
    //    }
}
