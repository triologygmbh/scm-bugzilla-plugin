/**
 * (c) 2012 TRIOLOGY GmbH
 */
package de.triology.scm.plugins.bugzilla;

import org.junit.Before;
import org.junit.Test;


/**
 * @author jboerner
 *
 */
public class XmlRpcBugzillaHandlerFactoryTest {

    /** The Bugzilla handler for handle request to the ticket system. */
    //        private BugzillaHandler handler;

    /**
     * @throws java.lang.Exception if login to Bugzilla server fails.
     */
    @Before
    public final void setUp() throws Exception {
	//    	final BugzillaHandlerFactory factory = new XmlRpcBugzillaHandlerFactory();
	//    	try {
	//    	    handler = factory.createBugzillaHandler("http://localhost", "scmadmin@localhost.local", "scmadmin");
	//    	} catch (final BugzillaConnectException e) {
	//    	    throw new Exception("Login to Bugzilla server failed.", e);
	//    	}
    }

    /**
     * Test method for
     * {@link de.triology.scm.plugins.bugzilla.XmlRpcBugzillaHandlerFactory#createBugzillaHandler(java.lang.String,
     *   java.lang.String, java.lang.String)}.
     */
    @Test
    public final void testCreateBugzillaHandler() {
	//    	assertNotNull(handler);
	//    	try {
	//    	    handler.addComment(2, "Testcomment from scm-bugzilla-plugin TestClass @ ".concat(
	//    		    String.valueOf(Calendar.getInstance().getTimeInMillis())));
	//    	    handler.logout();
	//    	} catch (final BugzillaException e) {
	//    	    e.printStackTrace();
	//    	    fail(e.getMessage());
	//    	}
    }
}
